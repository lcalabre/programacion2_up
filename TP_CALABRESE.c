#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define CONSTANTE_ASCII_RANDOM 97
#define CONSTANTE_LETRAS_AZ 26

//VARIABLES
int opcionMenuLogin;
int opcionMenuPrincipal;
int contadorIntentos = 0;

//FUNCIONES
void generarCaptcha();
void indexApp();
void login();
void menuLogin();
void mostrarMenuLogin();
void mostrarMenuPrincipal();
void opcion1();
void opcion2();
void opcion3();
void opcion4();

void login() {

    while(contadorIntentos<4) {
        system("cls");
        generarCaptcha();
    }
    system("cls");
    printf("AGOTO SUS INTENTOS!\n\n\n\n\n");

}

void menuLogin() {
    mostrarMenuLogin();
    printf("Seleccione una opcion: ");
    scanf("%d", &opcionMenuLogin);
    switch(opcionMenuLogin) {
    case 1 :
        system("cls");
        login();
        break;
    case 2 :
        system("cls");
        printf("Hasta pronto!\n\n\n\n\n");
    }

}

void generarCaptcha () {
    int vectorCaptcha[4];

    int i = 0, auxComparacion =0, hayDiferencias = 0, ingresados;

    printf("\t\t\t * * * CAPTCHA * * * \t\t\t\n\n\t\t\t\t");

    for(i=0; i<4; i++) {
        //GENERO NUMERO ALEATORIO EQUIVALENTE A LETRA MINUSCULA ASCII
        int captchAux = rand() %CONSTANTE_LETRAS_AZ + CONSTANTE_ASCII_RANDOM;
        putchar(captchAux);
        vectorCaptcha[i] = captchAux;
    }

    fflush(stdin);

    printf("\n\nIngrese los caracteres en pantalla: ");

    ingresados = getchar();
    //MIENTRAS QUE EL FLAG DE DIFERENTE ESTE EN CERO Y POSICIONES DEL VECTOR SIN COMPARAR
    while(hayDiferencias != 1 && auxComparacion<4) {
        //VALIDO CADA LETRA DE CAPTCHA
        if(ingresados == vectorCaptcha[auxComparacion]) {
            ingresados = getchar();
            auxComparacion++;
        } else {
            hayDiferencias = 1;
            contadorIntentos++;
        }
    }

    if(hayDiferencias==0) {
        indexApp();
    }

    //LIMPIO EL BUFFER DEL INPUT
    while ((ingresados = getchar()) != '\n' && ingresados != EOF) { }


}

void mostrarMenuPrincipal() {
    printf("\t\t\t * * * MENU PRINCIPAL * * * \t\t\t\n");
    printf("1)OPCION 1\n");
    printf("2)OPCION 2\n");
    printf("3)OPCION 3\n");
    printf("4)OPCION 4\n");
    printf("5)SALIR\n\n");
}

void mostrarMenuLogin() {
    printf("\t\t\t * * * BIENVENIDO * * * \t\t\t\n");
    printf("1)INGRESO CON CLAVE\n");
    printf("2)SALIR\n\n");
}

void indexApp() {
    while(opcionMenuPrincipal<5 || opcionMenuPrincipal>1) {
        system("cls");
        mostrarMenuPrincipal();
        scanf("%d", &opcionMenuPrincipal);

        switch(opcionMenuPrincipal) {
        case 1 :
            //TODO IMPLEMENTAR
            opcion1();
            break;

        case 2 :
            //TODO IMPLEMENTAR
            opcion2();
            break;

        case 3 :
            //TODO IMPLEMENTAR
            opcion3();
            break;

        case 4 :
            //TODO IMPLEMENTAR
            opcion4();
            break;

        case 5 :
            system("cls");
            exit(0);
            break;
        }
    }
}

void opcion1() {
    system("cls");
    printf("\t\t\t * * * OPCION 1 * * * \t\t\t\n");
    //TODO IMPLEMENTAR FUNCIONALIDAD
    printf("Opcion 1 en construccion...\n\n");
    system("pause");
}
void opcion2() {
    system("cls");
    printf("\t\t\t * * * OPCION 2 * * * \t\t\t\n");
    //TODO IMPLEMENTAR FUNCIONALIDAD
    printf("Opcion 2 en construccion...\n\n");
    system("pause");
}
void opcion3() {
    system("cls");
    printf("\t\t\t * * * OPCION 3 * * * \t\t\t\n");
    //TODO IMPLEMENTAR FUNCIONALIDAD
    printf("Opcion 3 en construccion...\n\n");
    system("pause");
}
void opcion4() {
    system("cls");
    printf("\t\t\t * * * OPCION 4 * * * \t\t\t\n");
    //TODO IMPLEMENTAR FUNCIONALIDAD
    printf("Opcion 4 en construccion...\n\n");
    system("pause");

}

int main() {
    //SEMILLA PARA RANDOMIZAR EL TIEMPO DEL CAPTCHA EN CADA EJECUCION
    srand(time(NULL));
    menuLogin();
}
